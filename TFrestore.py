import tensorflow as tf
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
import tflearn
#this can be downloaded from gethub and added to the project directory
import data_helper
from keras.utils import to_categorical
from keras.preprocessing.sequence import skipgrams, pad_sequences
from random import shuffle

##Section 1 ##########################################################################
##The following code deals with processing and preparing our data from the json file #
######################################################################################


#Using pandas here to read and handle json data, in this example using data from amazon
#df_Movie = pd.read_json("Movies_and_TV.json", lines=True)
df = pd.read_json("Games.json", lines=True)
#df was df_Game
#This will Concat the data - which I do not need 
#if you would like to use more data sets you will need to use this
#df = pd.concat([df_Movie, df_Games])


#TEMP statment for testing limits 
#df = df[:30000]
#erase on final product, for testing

df["label"] = np.where(df.overall >=3, 1, 0)

#here we are going to split the data so that pos and neg reviews are simular in size
#for the training model 
num_to_sample = len(df[df.label == 0])

df_neg = df[df["label"] == 0].sample(n=num_to_sample)
df_pos = df[df["label"] == 1].sample(n=num_to_sample)

df = pd.concat([df_neg, df_pos])

#this line will get the text from the data frames which is what we need to continue
text = df["reviewText"].values

labels = df["label"].values

##data, word_to_idx, idx_to_word, T = data_helper.tokenize_and_process(text, vocab_size = vocab_size)
##moved
######code removed see text


#Section 2
#The following code will be used to setup nerual network
#This creates the model that will be used in the next section of code

vocab_size = 2000

embedding_size = 256
num_samples = 64
learning_rate = 0.001

# Number is hidden units
lstm_hidden_units = 256

#Num of classes
num_classes = 2
#Num of words in each sequence
sequence_length = 250


if usr_option == "1":
	restore = False
elif usr_option == "2":
	restore = True
else:
	print("Wrong input. Will now exit")
	exit()

data, word_to_idx, idx_to_word, T = data_helper.tokenize_and_process(text, vocab_size = vocab_size)

#positive =[1,0]
#negative = [0,1]
#original_sequence = [234, 43, 12, 634, 123]
#padded_sequence = [0,0,0,0...,0,234, 43, 12, 634, 123]
#comment out as the following library does the padding and sequence setting

data = pad_sequences(data, maxlen = sequence_length)
labels = to_categorical(labels, num_classes = num_classes)



##deleted previous code which illistrated doing word_2_vec manualy
 
def model():
	x = tf.placeholder(tf.int32, shape=[None, sequence_length], name = "x")
	y = tf.placeholder(tf.int32, shape=[None, num_classes], name = "y")
	
	#cast label as float for math
	y = tf.cast(y, tf.float32)

	#instatiate embedding matrix
	Embedding = tf.Variable(tf.random_uniform([vocab_size, embedding_size], -1.0, 1.0), name="word_embedding")

	#embedding lookup
	embed_lookup = tf.nn.embedding_lookup(Embedding, x)
	
	#create cell required for tenserflow, creates a LSTM (long-short term memory) cell
	lstm_cell = tf.contrib.rnn.BasicLSTMCell(lstm_hidden_units)

	#evaluate shape to get batch size of variable
	current_batch_size = tf.shape(x)[0]

	#creating LSTM with initial state of 0's
	initial_state = lstm_cell.zero_state(current_batch_size, dtype = tf.float32)

	#this will wrap the lstm cell in a dropout wrapper to avoid overfitting
	lstm_call = tf.contrib.rnn.DropoutWrapper(cell = lstm_cell, output_keep_prob=0.85)

	# the _ refers to the hidden cells state, its kinda a throwaway 
	value, _ = tf.nn.dynamic_rnn(lstm_cell, embed_lookup, initial_state = initial_state, dtype = tf.float32)

	#Instaniate weight
	weight = tf.Variable(tf.random_normal([lstm_hidden_units, num_classes]))
	#Instantiate bias
	bias = tf.Variable(tf.constant(0.1, shape=[num_classes]))

	#transpose and transform, this manipulates the shape
	value = tf.transpose(value, [1,0,2])

	#extract final output for prediction
	last = tf.gather(value, int(value.get_shape()[0])-1)

	prediction = (tf.matmul(last, weight) + bias)
	
	#comparing prediction and labels for example see comments below
	#prediction =   [1,1,0,0]
	#labels =       [1,0,0,1]
	#correct_pred = [1,0,1,0]
	correct_prediction = tf.equal(tf.argmax(tf.nn.sigmoid(prediction), axis =1), tf.argmax(y, axis=1))
	accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

	#choice the model made
	choice = tf.argmax(tf.nn.sigmoid(prediction), axis =1), tf.argmax(y, axis=1)

	#Calc loss given prediction and labels
	loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits = prediction, labels = y))

	optimizer = tf.train.RMSPropOptimizer(learning_rate = learning_rate).minimize(loss)

	return optimizer, loss, x, y, accuracy, prediction, correct_prediction, choice

batch_size = 32
num_epochs = 5

X_train, testx, y_train, testy = train_test_split(data, labels, test_size = 0.3, random_state=42)

optimizer, loss, x, y, accuracy, pred, correct_pred, choice = model()

num_batches = len(X_train) // batch_size

sesh = tf.Session()

init = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
sesh.run(init)

#create a saver and a writer
saver = tf.train.Saver()
writer = tf.summary.FileWriter("logdir/", graph = sesh.graph)

if restore:
	saver.restore(sesh, "logdir\\lstm_model.ckpt")

def train_model(X_train, y_train):
	for epoch in range(num_epochs):
		print("+++ Epoch ->", epoch+1, " - out of ->", num_epochs, "+++")
		if epoch > 0:
			data = list(zip(X_train, y_train))
			shuffle(data)
			X_train, y_train = zip(*data)

		for i in range(num_batches):
			if i != num_batches -1:
				x_batch = X_train[i * batch_size : i * batch_size + batch_size]
				y_batch = y_train[i * batch_size : i * batch_size + batch_size]
			else:
				x_batch = X_train[i * batch_size:]
				y_batch = y_train[i * batch_size:]
			_, l, a = sesh.run([optimizer, loss, accuracy], feed_dict={x: x_batch, y: y_batch})
 
			if i > 0 and i % 100 == 0:
				print("Step ", i, " of ", num_batches, " loss: ", l, "Accuracy: ", a)
			if i > 0 and i % 500 == 0:
				saver.save(sesh, "logdir\\lstm_model.ckpt")
				writer.flush()
				writer.close()
		saver.save(sesh, "logdir\\lstm_model.ckpt")
		
		writer.flush()
		writer.close()
