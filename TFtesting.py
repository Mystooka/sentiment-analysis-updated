import tensorflow as tf
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
import tflearn
#this can be downloaded from gethub and added to the project directory
import data_helper
from keras.utils import to_categorical
from keras.preprocessing.sequence import skipgrams, pad_sequences
from random import shuffle

##Section 1 ##########################################################################
##The following code deals with processing and preparing our data from the json file #
######################################################################################


#Using pandas here to read and handle json data, in this example using data from amazon
#df_Movie = pd.read_json("Movies_and_TV.json", lines=True)
df = pd.read_json("Games.json", lines=Terue)
#df was df_Game
#This will Concat the data - which I do not need 
#df = pd.concat([df_Movie, df_Games])

#this line will get the text from the data frames which is what we need to continue
text = df["reviewText"].values

#TEMP statment for testing reduces memory
df = df[:30000]
#erase on final product

df["label"] = np.where(ds.overall >=3, 1, 0)

#here we are going to split the data so that pos and neg reviews are simular in size
#for the training model 
num_to_sample = len(df[df.label == 0])

#Reduce memory
del df

#can also reduce memory by adding in df = df.sample(10000)  <-sample size
#df = df.sample(1000)

text = [data_helper.clean_str(sent) for sent in text]

#set max sentence length
max_sent_length = 100

vocab_proc = tflearn.data_utils.VocabularyProcessor(max_sent_length)
data = list(vocab_proc.fit_transform(text))

vocab_size = len(vocab_proc.vocabulary_)

#convert numpy lists to python lists
data = [i.tolist() for i in data]

##get rid of trailing 0s
for lst in data:
	try:
		while lst[-1] == 0:
			lst.pop()
	except:
		pass
#filter the empty lists
data = filter(None, data)
#Covert data to numpy list to python list
data = np.array(list(data)) 

window = 2

pivot_words = []
target_words = []

for d in range(data.shape[0]):
	pivot_idx = data[d][window:-window]

	for i in range(len(pivot_idx)):
		pivot = pivot_idx[i]

		targets = np.array([])

		neg_target = data[d][i : window+i]
		pos_target = data[d][i + window +1: i + window + window +1]
		targets = np.append(targets, [neg_target, pos_target]).flatten().tolist()

		for c in range(window*2):
			pivot_words.append(pivot)
			target_words.append(targets[c])
#Section 2
#The following code will be used to setup nerual network
#This creates the model that will be used in the next section of code

embedding_size = 128
num_samples = 64
learning_rate = 0.001

def build_word2vec():
	#pivot words then target
	x = tf.placeholder(tf.int32, shape=[None,], name="x_pivot_idxs")
	y = tf.placeholder(tf.int32, shape=[None,], name="y_target_idxs")

##makes embedding matrix for words
	Embedding = tf.Variable(tf.random_uniform([vocab_size, embedding_size], -1.0, 1.0), name="word_embedding")
##sets up weights and Biases for NCE loss	
	nce_weights = tf.Variable(tf.truncated_normal([vocab_size, embedding_size], stddev=tf.sqrt(1/embedding_size)), name="nce_weights")

	nce_biases = tf.Variable(tf.zeros([vocab_size]), name="nce_biases")

	pivot = tf.nn.embedding_lookup(Embedding, x, name="word_embed_lookup")

	train_labels = tf.reshape(y, [tf.shape(y)[0], 1])

	loss = tf.reduce_mean(tf.nn.nce_loss(weights = nce_weights, biases = nce_biases, labels = train_labels, inputs = pivot, num_sampled = num_samples, num_classes = vocab_size, num_true = 1))

	optimizer = tf.contrib.layers.optimize_loss(loss, tf.train.get_global_step(), learning_rate, "Adam", clip_gradients = 5.0, name="Optimizer")
	sesh = tf.Session()
	sesh.run(tf.global_variables_initializer())

	return optimizer, loss, x, y, sesh
##the code above deals with the word2vec portion and setting up neural net
optimizer, loss, x, y, sesh = build_word2vec()

#Section 3
#Now that the model is created we need to do some training with the data
#that was prepared in the first section

X_train, X_test, y_train, y_test = train_test_split(pivot_words, target_words)

batch_size = 32
num_epochs = 5

#Number of batches in training sets
num_batches = len(X_train) // batch_size

#this saver is used to save the weights
saver = tf.train.Saver()

for e in range(num_epochs):
	for i in range(num_batches):
		if i != range(num_batches-1):
			x_batch = X_train[i*batch_size: i * batch_size + batch_size]
			y_batch = y_train[i*batch_size: i * batch_size + batch_size]
		else:
			x_batch = X_train[i*batch_size:]
			y_batch = y_train[i*batch_size:]
		_, l = sesh.run([optimizer, loss], feed_dict = {x: x_batch, y: y_batch})
		if i>0 and i %1000 == 0:
			print("Step --> ", i, "of", num_batches, "loss --> ", 1)
	#save_path = saver.save(sesh, "logdir\\word2vec_model.ckpt")

##ended after 3rd video 